# Description

This package contains various customs msgs and services used in different packages.

# Service

## CPP

Here an example on how to use a service in C++.

### Server

In object VelocityControlTracking : 

```cpp

#include <msg_srv_action_gestion/Limits.h>


ros::ServiceServer get_limits = node_handle.advertiseService("/get_limits",  &VelocityControlTracking::getLimits,this);    

while (ros::ok())
{
    ros::spinOnce();
}


bool VelocityControlTracking::getLimits(msg_srv_action_gestion::Limits::Request &req,
               msg_srv_action_gestion::Limits::Response &res)
{

    if (true)
    {
        Eigen::Vector3d cartUpperLim(
            req.upperLimit.position.x,
            req.upperLimit.position.y,
            req.upperLimit.position.z
        );

        Eigen::Vector3d cartLowerLim(
            req.lowerLimit.position.x,
            req.lowerLimit.position.y,
            req.lowerLimit.position.z
        );    


        res.success = true;

        return(true);
    }
    else
    {
        res.success = false;
        return(false);
    }
}

```

### Client

```cpp
#include <msg_srv_action_gestion/Limits.h>

ros::ServiceClient client = node.serviceClient<msg_srv_action_gestion::Limits>("/get_limits");


bool worked = false;

while (!worked and ros::ok())
{

    msg_srv_action_gestion::Limits srv;

    srv.request.upperLimit.position.x = 0.1;
    srv.request.upperLimit.position.y = 0.1;
    srv.request.upperLimit.position.z = 0.1;

    srv.request.lowerLimit.position.x = -0.1;
    srv.request.lowerLimit.position.y = -0.1;
    srv.request.lowerLimit.position.z = -0.1;        

    bool send = false;
    if (client.call(srv))
    {
        ROS_INFO("All right!");
        worked = true;
    }

    ros::spinOnce();
}

```